export class Repository {
  constructor(public name: string, public url: string, public stars: number, public forks: number) {}
}
