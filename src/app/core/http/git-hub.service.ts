import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Repository} from '../model/repository';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class GitHubService {

  private ENDPOINT = 'https://api.github.com';

  private repositories: Repository[] = [];

  constructor(private http: HttpClient,
              private toastr: ToastrService) { }

  getRepos(username: string, page: number): Observable<Repository[]> {
    this.repositories = [];
    return this.http.get<any[]>(this.ENDPOINT + '/users/' + username + '/repos?page=' + page + '&per_page=10')
      .pipe(map(repos => {
        repos.forEach(repo => {
          const repository = new Repository(
            repo.name,
            repo.html_url,
            repo.stargazers_count,
            repo.forks
          );
          this.repositories.push(repository);
        });
        return this.repositories;
      }), catchError((error: HttpErrorResponse) => {
        if (error.status === 404) {
          this.toastr.error('No repositories found for user: ' + username, 'Not found');
        }
        return of(null);
      }));
  }
}
