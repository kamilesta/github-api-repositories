import {Component} from '@angular/core';
import {GitHubService} from '../core/http/git-hub.service';
import {Repository} from '../core/model/repository';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent {

  private username: string;
  private page = 1;
  private repositories: Repository[] = [];
  private isSearched = false;

  constructor(private githubService: GitHubService,
              private toastr: ToastrService) {
  }

  loadInitialData(username: string) {
    this.page = 1;
    this.username = username;
    this.isSearched = true;
    this.getRepos(username);
  }

  private getRepos(username: string) {
    this.githubService.getRepos(username, this.page).subscribe(repos => {
      if (repos.length === 0) {
        this.toastr.error('Repository for user: ' + username + ' is empty', 'Not found');
      }
      this.repositories = repos;
    });
  }

  loadMoreData() {
    this.page++;
    this.repositories = [];
    this.getRepos(this.username);
  }
}
