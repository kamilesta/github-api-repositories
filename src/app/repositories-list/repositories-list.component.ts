import {Component, Input} from '@angular/core';
import {Repository} from '../core/model/repository';

@Component({
  selector: 'app-repositories-list',
  templateUrl: './repositories-list.component.html',
  styleUrls: ['./repositories-list.component.css']
})
export class RepositoriesListComponent {

  @Input() repositories: Repository[];
  @Input() isSearched: boolean;

  constructor() { }
}
